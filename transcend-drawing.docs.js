/**
 * @ngdoc overview
 * @name transcend.drawing
 * @description
 # Drawing Module
 The "Drawing" module contains the components to add svg drawing capabilities to any container. Such drawing capabilities
 include; points, lines, rectangles, and text.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-drawing.git
 ```

 Other options:

 * Download the code at [http://code.tsstools.com/bower-drawing/get/master.zip](http://code.tsstools.com/bower-drawing/get/master.zip)
 * View the repository at [http://code.tsstools.com/bower-drawing](http://code.tsstools.com/bower-drawing)
 * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/drawing/?at=develop)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.drawing']);
 ```

 ## To Do
 - Add unit tests.
 - Explore other drawing libraries and see if there is a better solution.

 ## Project Goals
 - Keep this module size below 25kb minified
 */
/**
 * @ngdoc service
 * @name transcend.drawing.service:$svgCanvasGroup
 *
 * @description
 * The '$svgCanvasGroup' factory adds properties to the SVG Canvas.
 *

 *
 * @requires $window
 *
 */
/**
 * @ngdoc service
 * @name transcend.drawing.service:$vectorEditor
 *
 * @description
 * The '$vectorEditor' factory returns a new Vector Editor for SVG purposees.
 *

 *
 * @requires $window
 *
 */
/**
 * @ngdoc directive
 * @name transcend.drawing.directive:drawToolbar
 *
 * @description
 The 'drawToolbar' directive provides an editable toolbar for the SVG Canvas.
 *
 * @restrict EA
 *
 * @requires drawingConfig
 * @requires $svgCanvasGroup
 */
/**
 * @ngdoc directive
 * @name transcend.drawing.directive:svgCanvas
 *
 * @description
 The 'svgCanvas' directive provides a template for a Scalable Vectory Graphics (SvG) Canvas.
 *
 * @restrict EA
 * @scope
 *
 * @requires $rootScope
 * @requires $window
 * @requires $timeout
 * @requires $svgCanvasGroup
 * @requires settings
 * @requires $color
 * @requires $vectorEditor
 *
 */
