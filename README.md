# Drawing Module
The "Drawing" module contains the components to add svg drawing capabilities to any container. Such drawing capabilities
include; points, lines, rectangles, and text.

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-drawing.git
```

Other options:

* Download the code at [http://code.tsstools.com/bower-drawing/get/master.zip](http://code.tsstools.com/bower-drawing/get/master.zip)
* View the repository at [http://code.tsstools.com/bower-drawing](http://code.tsstools.com/bower-drawing)
* If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/drawing/?at=develop)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.drawing']);
```

## To Do
- Add unit tests.
- Explore other drawing libraries and see if there is a better solution.

## Project Goals
- Keep this module size below 25kb minified